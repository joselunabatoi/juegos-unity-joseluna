using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InterfaceFinal : MonoBehaviour
{
    public Text textThrown;
    public Text textDestroyed ;   


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        textThrown.text = "Number of balls: "+GameManager.currentNumberStonesThrown;
        textDestroyed.text = "Number of goals: "+GameManager.currentNumberDestroyedStones;   
    }

    public void Click(){
    SceneManager.LoadScene("Scena2");

    }


}
