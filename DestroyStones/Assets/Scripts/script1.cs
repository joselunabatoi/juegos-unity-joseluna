using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class script1 : MonoBehaviour
{
    private const float yDie = -30.0f;   

    void Start()
    {
        
    }

    void Update()
    {
        if(transform.position.y < yDie){
            Destroy(gameObject);
        }
    }

    private void OnMouseDown(){
        Destroy(gameObject);
        GameManager.currentNumberDestroyedStones++;
    }

}
