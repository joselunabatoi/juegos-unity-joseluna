using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stone : MonoBehaviour
{
    public float impulseForce = 10.0f;
    
    void Start()
    {
        GetComponent<Rigidbody>().AddForce(Vector3.up * impulseForce,ForceMode.Impulse);
    }

    
    void Update()
    {
        if (transform.position.y < -1.0f){
            Destroy(gameObject);
        }
    }
}
